{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Defining and Using Functions and Classes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So far, our scripts have been simple, single-use code blocks.\n",
    "One way to organize our Python code and to make it more readable and reusable is to factor-out useful pieces into reusable *functions*.\n",
    "Here we'll cover two ways of creating functions: the ``def`` statement, useful for any type of function, and the ``lambda`` statement, useful for creating short anonymous functions."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 📖Using Functions\n",
    "\n",
    "Functions are groups of code that have a name, and can be called using parentheses.\n",
    "We've seen functions before. For example, ``print`` in Python is a function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('abc')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here ``print`` is the function name, and ``'abc'`` is the function's *argument*.\n",
    "\n",
    "In addition to arguments, there are *keyword arguments* that are specified by name.\n",
    "One available keyword argument for the ``print()`` function is ``sep``, which tells what character or characters should be used to separate multiple items:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(1, 2, 3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(1, 2, 3, sep='--')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When non-keyword arguments are used together with keyword arguments, the keyword arguments must come at the end."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 📖Defining Functions\n",
    "Functions become even more useful when we begin to define our own, organizing functionality to be used in multiple places.\n",
    "In Python, functions are defined with the ``def`` statement.\n",
    "For example, we can encapsulate a version of our Fibonacci sequence code from the previous section as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def fibonacci(N):\n",
    "    L = []\n",
    "    a, b = 0, 1\n",
    "    while len(L) < N:\n",
    "        a, b = b, a + b\n",
    "        L.append(a)\n",
    "    return L"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we have a function named ``fibonacci`` which takes a single argument ``N``, does something with this argument, and ``return``s a value; in this case, a list of the first ``N`` Fibonacci numbers:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fibonacci(10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Python functions can return any Python object, simple or compound, which means constructs are straightforward in Python.\n",
    "\n",
    "For example, multiple return values are simply put in a tuple, which is indicated by commas:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def real_imag_conj(val):\n",
    "    return val.real, val.imag, val.conjugate()\n",
    "\n",
    "r, i, c = real_imag_conj(3 + 4j)\n",
    "print(r, i, c)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 📖Default Argument Values\n",
    "\n",
    "Often when defining a function, there are certain values that we want the function to use *most* of the time, but we'd also like to give the user some flexibility.\n",
    "In this case, we can use *default values* for arguments.\n",
    "Consider the ``fibonacci`` function from before.\n",
    "What if we would like the user to be able to play with the starting values?\n",
    "We could do that as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def fibonacci(N, a=0, b=1):\n",
    "    L = []\n",
    "    while len(L) < N:\n",
    "        a, b = b, a + b\n",
    "        L.append(a)\n",
    "    return L"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With a single argument, the result of the function call is identical to before:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fibonacci(10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But now we can use the function to explore new things, such as the effect of new starting values:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fibonacci(10, 0, 2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The values can also be specified by name if desired, in which case the order of the named values does not matter:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fibonacci(10, b=3, a=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### ✏️Task:\n",
    "Define the following function as a Python function with default value $x = 2$:\n",
    "\n",
    "$test(x) = 3^x - 7x$\n",
    "\n",
    "and print the value for:\n",
    "\n",
    "$y = test(5)$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your solution here:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Sample solution\n",
    "\n",
    "def test(x=2):\n",
    "    return 3**x - 7*x\n",
    "\n",
    "y = test(5)\n",
    "print(\"y =\", y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 📖Anonymous (``lambda``) Functions\n",
    "Earlier we quickly covered the most common way of defining functions, the ``def`` statement.\n",
    "You'll likely come across another way of defining short, one-off functions with the ``lambda`` statement.\n",
    "It looks something like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "add = lambda x, y: x + y\n",
    "add(1, 2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This lambda function is roughly equivalent to"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def add(x, y):\n",
    "    return x + y"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So why would you ever want to use such a thing?\n",
    "Primarily, it comes down to the fact that *everything is an object* in Python, even functions themselves!\n",
    "That means that functions can be passed as arguments to functions.\n",
    "\n",
    "As an example of this, suppose we have some data stored in a list of dictionaries:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data = [{'first':'Guido', 'last':'Van Rossum', 'YOB':1956},\n",
    "        {'first':'Grace', 'last':'Hopper',     'YOB':1906},\n",
    "        {'first':'Alan',  'last':'Turing',     'YOB':1912}]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now suppose we want to sort this data.\n",
    "Python has a ``sorted`` function that does this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sorted([2,4,3,5,1,6])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But dictionaries are not orderable: we need a way to tell the function *how* to sort our data.\n",
    "We can do this by specifying the ``key`` function, a function which given an item returns the sorting key for that item:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# sort alphabetically by first name\n",
    "sorted(data, key=lambda item: item['first'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# sort by year of birth\n",
    "sorted(data, key=lambda item: item['YOB'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "While these key functions could certainly be created by the normal, ``def`` syntax, the ``lambda`` syntax is convenient for such short one-off functions like these."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### ✏️Task:\n",
    "Now implement the test function, which you defined earlier, as an anonymous function using a *lambda* expression."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your solution here:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Sample solution\n",
    "\n",
    "test_lambda = lambda x: 3**x - 7*x\n",
    "\n",
    "result = test_lambda(5)\n",
    "\n",
    "print(result)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### ✏️Task:\n",
    "Define  a function called $multiplier$ that takes a factor`n`, and returns a function that multiplies an argument, `x` , by `n`.\n",
    "\n",
    "Test the $multiplier$ function with $n= 3$ and the resulting function with $x = 7$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your solution here:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Sample solution\n",
    "\n",
    "def multiplier(n):\n",
    "    def inner(x):\n",
    "        return x * n\n",
    "    return inner\n",
    "\n",
    "print(multiplier(3)(7))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 📖``*args`` and ``**kwargs``: Flexible Arguments\n",
    "Sometimes you might wish to write a function in which you don't initially know how many arguments the user will pass.\n",
    "In this case, you can use the special form ``*args`` and ``**kwargs`` to catch all arguments that are passed.\n",
    "Here is an example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def catch_all(*args, **kwargs):\n",
    "    print(\"args =\", args)\n",
    "    print(\"kwargs = \", kwargs)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "catch_all(1, 2, 3, a=4, b=5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "catch_all('a', keyword=2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here it is not the names ``args`` and ``kwargs`` that are important, but the ``*`` characters preceding them.\n",
    "``args`` and ``kwargs`` are just the variable names often used by convention, short for \"arguments\" and \"keyword arguments\".\n",
    "The operative difference is the asterisk characters: a single ``*`` before a variable means \"expand this as a sequence\", while a double ``**`` before a variable means \"expand this as a dictionary\".\n",
    "In fact, this syntax can be used not only with the function definition, but with the function call as well!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "inputs = (1, 2, 3)\n",
    "keywords = {'pi': 3.14}\n",
    "\n",
    "catch_all(*inputs, **keywords)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 📖Using Classes\n",
    "A class is like a factory or a blueprint, that describes what something should be, but it isn't that thing itself. It defines how to build a particular type of object. It specifies the attributes and methods that will be common to all objects created using it. For example, a class for creating cars might define attributes like color, and model, and methods like .start() and .park().\n",
    "\n",
    "An instance is an individual object created from a class. It's a single realization of the class, with specific values for the attributes defined in the class. Using the car class example, one instance might be a blue Honda Civic, while another might be a red Toyota Camry. Each car is a separate instance, or object, created from the same class.\n",
    "\n",
    "An attribute is a property or characteristic of an object, accessed using dot notation. In the car example, color, and model are all attributes. You can access an object's attribute using the dot notation, such as `car.color`, which might give you the value \"blue\" for a specific car instance.\n",
    "\n",
    "A method is a function within a class that defines an action objects can perform, called using dot notation with parentheses, like `car.start()`, which might trigger the action of starting the car's engine.\n",
    "\n",
    "This concept allows you to create reusable and organized code structures."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 📖Defining Classes\n",
    "Just like creating your own functions, you can also create your own classes. You can define a class in Python using the $class$ keyword. It is customary to begin the name of a class with a capital letter to differentiate it from variables. Here's a simple example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Cat:\n",
    "    def meow (self):\n",
    "        print(\"meow! meoow! meooow!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once you've defined a class, you can create instances of that class. Here's how you create an instance of the `Cat` class:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "garfield = Cat()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can call methods on an instance of a class just like you would call a function. Here's how you would call the `meow` method on `garfield`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "garfield.meow()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 📖`__init__` method \n",
    "Inside many classes, you'll find a method called `__init__`.The `__init__` method is a special method (methods that are surrounded by double underscores) that is used for initializing new objects created from a class. \"Initializing\" means setting up the object with specific attributes when it's created.\n",
    "\n",
    "\n",
    "When you create a new object from a class that has an `__init__` method, Python automatically looks for this method inside the class and calls it, i.e, the code inside the `__init__` method is run automatically. You pass in any necessary parameters along with a special parameter called `self`, which refers to the object being created.\n",
    "\n",
    "`self` is a reference to the specific object that the method is being called on, allowing you to access and manipulate the properties of that object. You can choose another name instead of self, but using self is standard practice; it's simply a placeholder for the object itself.\n",
    "\n",
    "Here's a code example to demonstrate these concepts:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Cat:\n",
    "    def __init__(self, name, breed):\n",
    "        self.name = name\n",
    "        self.breed = breed\n",
    "    \n",
    "    def meow (self):\n",
    "        print(self.name + \" said: meow! meoow! meooow!\")\n",
    "\n",
    "my_cat = Cat(name='garfield', breed='Persian longhair')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `Cat` class has an `__init__` method that takes three parameters: `self`, `name`, and `breed`. When you create `my_cat`, Python automatically passes `my_cat` as `self` and takes the `name` and `breed`. The attributes are set on `my_cat` using the values you passed in.\n",
    "\n",
    "Here's an example that shows how to use the created object:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Accessing attributes\n",
    "my_cat.name"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Calling a method\n",
    "my_cat.meow()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### ✏️Task:\n",
    "Create a class `Rectangle` that gets two values as initialization: `width` and `height` to represent the dimensions of a rectangle. Implement a method 'area()' that calculates the area of the rectangle. If `r` is your Rectangle-object, you should be able to determine its area by calling \"r.area()\"."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your solution here:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Sample solution\n",
    "\n",
    "class Rectangle:\n",
    "    def __init__(self, width, height):\n",
    "        self.width = width\n",
    "        self.height = height\n",
    "\n",
    "    def area(self):\n",
    "        return self.width * self.height\n",
    "\n",
    "r = Rectangle(7, 11)\n",
    "print(\"Area of the rectangle:\", r.area())"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
