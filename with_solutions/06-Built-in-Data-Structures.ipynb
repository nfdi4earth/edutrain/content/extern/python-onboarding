{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Built-In Data Structures"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We have seen Python's simple types: ``int``, ``float``, ``complex``, ``bool``, ``str``, and so on.\n",
    "Python also has several built-in compound types, which act as containers for other types.\n",
    "These compound types are:\n",
    "\n",
    "| Type Name | Example                   |Description                            |\n",
    "|-----------|---------------------------|---------------------------------------|\n",
    "| ``list``  | ``[1, 2, 3]``             | Ordered collection                    |\n",
    "| ``tuple`` | ``(1, 2, 3)``             | Immutable ordered collection          |\n",
    "| ``dict``  | ``{'a':1, 'b':2, 'c':3}`` | Unordered (key,value) mapping         |\n",
    "| ``set``   | ``{1, 2, 3}``             | Unordered collection of unique values |\n",
    "\n",
    "As you can see, round, square, and curly brackets have distinct meanings when it comes to the type of collection produced.\n",
    "We'll take a quick tour of these data structures here."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 📖Lists\n",
    "Lists are the basic *ordered* and *mutable* data collection type in Python.\n",
    "They can be defined with comma-separated values between square brackets; for example, here is a list of the first several prime numbers:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "L = [2, 3, 5, 7]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lists have a number of useful properties and methods available to them.\n",
    "Here we'll take a quick look at some of the more common and useful ones:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Length of a list\n",
    "len(L)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Append a value to the end\n",
    "L.append(11)\n",
    "L"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Addition concatenates lists\n",
    "L + [13, 17, 19]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# sort() method sorts in-place\n",
    "L = [2, 5, 1, 6, 3, 4]\n",
    "L.sort()\n",
    "L"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In addition, there are many more built-in list methods; they are well-covered in Python's [online documentation](https://docs.python.org/3/tutorial/datastructures.html).\n",
    "\n",
    "While we've been demonstrating lists containing values of a single type, one of the powerful features of Python's compound objects is that they can contain objects of *any* type, or even a mix of types. For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "L = [1, 'two', 3.14, [0, 3, 5]]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This flexibility is a consequence of Python's dynamic type system.\n",
    "We see that lists can even contain other lists as elements.\n",
    "Such type flexibility is an essential piece of what makes Python code relatively quick and easy to write.\n",
    "\n",
    "So far we've been considering manipulations of lists as a whole; another essential piece is the accessing of individual elements.\n",
    "This is done in Python via *indexing* and *slicing*, which we'll explore next."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 📖List indexing and slicing\n",
    "Python provides access to elements in compound types through *indexing* for single elements, and *slicing* for multiple elements.\n",
    "As we'll see, both are indicated by a square-bracket syntax.\n",
    "Suppose we return to our list of the first several primes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "L = [2, 3, 5, 7, 11]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Python uses *zero-based* indexing, so we can access the first and second element in using the following syntax:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "L[0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "L[1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Elements at the end of the list can be accessed with negative numbers, starting from -1:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "L[-1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "L[-2]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can visualize this indexing scheme this way:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![List Indexing Figure](https://github.com/Farzaneh-prime/introduction_to_python/blob/master/fig/list-indexing.png?raw=true)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here values in the list are represented by large numbers in the squares; list indices are represented by small numbers above and below.\n",
    "In this case, ``L[2]`` returns ``5``, because that is the next value at index ``2``."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Where *indexing* is a means of fetching a single value from the list, *slicing* is a means of accessing multiple values in sub-lists.\n",
    "It uses a colon to indicate the start point (inclusive) and end point (non-inclusive) of the sub-array.\n",
    "For example, to get the first three elements of the list, we can write:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "L[0:3]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice where ``0`` and ``3`` lie in the preceding diagram, and how the slice takes just the values between the indices.\n",
    "If we leave out the first index, ``0`` is assumed, so we can equivalently write:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "L[:3]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Similarly, if we leave out the last index, it defaults to the length of the list.\n",
    "Thus, the last three elements can be accessed as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "L[-3:]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, it is possible to specify a third integer that represents the step size; for example, to select every second element of the list, we can write:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "L[::2]  # equivalent to L[0:len(L):2]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A particularly useful version of this is to specify a negative step, which will reverse the array:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "L[::-1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Both indexing and slicing can be used to set elements as well as access them.\n",
    "The syntax is as you would expect:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "L[0] = 100\n",
    "print(L)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "L[1:3] = [55, 56]\n",
    "print(L)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A very similar slicing syntax is also used in many data science-oriented packages, including NumPy and Pandas (mentioned in the introduction).\n",
    "\n",
    "Now that we have seen Python lists and how to access elements in ordered compound types, let's take a look at the other three standard compound data types mentioned earlier."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "vpKGW0HCVUGz"
   },
   "source": [
    "### ✏️Task:\n",
    "Define the two lists \n",
    "\n",
    "$A = [1,2,3]$\n",
    "\n",
    "$B = [6,5,4]$\n",
    "\n",
    "and concatenate them, store the result in $C$ and print out $C$ and the number of elements in $C$ using the *len()* function.\n",
    "\n",
    "💡You can concatenate lists using the + operator in Python, which merges the elements of the two lists into a new list. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your solution here:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Sample solution\n",
    "\n",
    "A = [1, 2, 3]\n",
    "B = [6, 5, 4]\n",
    "\n",
    "C = A + B\n",
    "\n",
    "print(\"Concatenated list C:\", C)\n",
    "print(\"Number of elements in C:\", len(C))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "CfrGSoWpVUGz"
   },
   "source": [
    "Now extract the sublist $D = [2,3,6]$ from $C$ using indexing\n",
    "\n",
    "💡The start index in Python slicing is inclusive and the end index is exclusive."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your solution here:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Sample solution\n",
    "\n",
    "D = C[1:4]\n",
    "\n",
    "print(\"Sublist D:\", D)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "b_Wgg-bZVUG0"
   },
   "source": [
    "Insert element '$0$' to list $D$ behind element '$3$'.\n",
    "\n",
    "💡In Python, you can use the `insert()` function to add an element to a specific index in a list. The insert() function takes two parameters: the index at which the new element should be inserted, and the element itself."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your solution here:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Sample solution\n",
    "\n",
    "D.insert(1, 0)\n",
    "\n",
    "print(\"Updated list D:\", D)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "gEBtpH6RVUG0"
   },
   "source": [
    "Append element '$37$' to List $D$ and print the length of $D$.\n",
    "\n",
    "💡Appending an element to a list in Python is done using the `append()` method, which adds the element at the end of the list. To get the length (number of elements) of a list, we use the `len()` function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your solution here:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Sample solution\n",
    "\n",
    "D.append(37)\n",
    "\n",
    "print(\"Updated list D:\", D)\n",
    "print(\"Length of D:\", len(D))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "QUOTgtioVUG1"
   },
   "source": [
    "Remove elements '$37$' from List $D$, print $D$ and its length.\n",
    "\n",
    "💡You can use the `remove()` method of a list in Python to remove an element. Note that `remove()` only removes the first occurrence of the element. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your solution here:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Sample solution\n",
    "\n",
    "D.remove(37)\n",
    "\n",
    "print(\"Updated list D:\", D)\n",
    "print(\"Length of D:\", len(D))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "l9gCONNbVUG2"
   },
   "source": [
    "Sort list $D$ and print the result, you can use the `sort()` function to sort lists in Python. \n",
    "\n",
    "💡The `sort()` function sorts lists in ascending order. If you want to sort a list in descending order, you can use `sort(reverse=True)`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your solution here:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Sample solution\n",
    "\n",
    "D.sort()\n",
    "\n",
    "print(\"Sorted list D:\", D)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "OuML9FZiVUG2"
   },
   "source": [
    "Replace the second element in the list with the value $1973$.\n",
    "\n",
    "💡You can replace an element in a list by assigning a new value to the specific index. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your solution here:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Sample solution\n",
    "\n",
    "D[1] = 1973\n",
    "\n",
    "print(\"Updated list D:\", D)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 📖Tuples\n",
    "Tuples are in many ways similar to lists, but they are defined with parentheses rather than square brackets:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t = (1, 2, 3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "They can also be defined without any brackets at all:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t = 1, 2, 3\n",
    "print(t)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Like the lists discussed before, tuples have a length, and individual elements can be extracted using square-bracket indexing:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "len(t)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t[0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The main distinguishing feature of tuples is that they are *immutable*: this means that once they are created, their size and contents cannot be changed:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t[1] = 4"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t.append(4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Tuples are often used in a Python program; a particularly common case is in functions that have multiple return values.\n",
    "For example, the ``as_integer_ratio()`` method of floating-point objects returns a numerator and a denominator; this dual return value comes in the form of a tuple:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 0.125\n",
    "x.as_integer_ratio()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These multiple return values can be individually assigned as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "numerator, denominator = x.as_integer_ratio()\n",
    "print(numerator / denominator)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The indexing and slicing logic covered earlier for lists works for tuples as well, along with a host of other methods.\n",
    "Refer to the online [Python documentation](https://docs.python.org/3/tutorial/datastructures.html) for a more complete list of these."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "id": "Q4H2lGV8VUG3"
   },
   "outputs": [],
   "source": [
    "A = (1, 2, 3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "GTMqVBYjVUG3"
   },
   "source": [
    "### ✏️Task:\n",
    "Define two tuples, one directly and the other one from a list.\n",
    "\n",
    "$X = (10,20,30)$\n",
    "\n",
    "$Y = (40,50,60)$\n",
    "\n",
    "💡You can convert a list to a tuple using the `tuple()` function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your solution here:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Sample solution\n",
    "\n",
    "X = (10, 20, 30)  \n",
    "\n",
    "list_Y = [40, 50, 60]\n",
    "\n",
    "Y = tuple(list_Y)\n",
    "\n",
    "print(X)\n",
    "print(Y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "eYGwFkomVUG3"
   },
   "source": [
    "Extract the third element of tuple '$X$' and print it.\n",
    "\n",
    "💡In Python, we can access elements of a tuple (or any other iterable) using indexing. Indexing starts from 0."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your solution here:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Sample solution\n",
    "\n",
    "X = (10, 20, 30)\n",
    "\n",
    "third_element = X[2]\n",
    "\n",
    "print(third_element)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "bPby1khNVUG4"
   },
   "source": [
    "Use *tuple unpacking* to unpack '$A$' into the variables '$a,b,c$' in a single operation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your solution here:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Sample solution\n",
    "\n",
    "x, y, z = Y\n",
    "\n",
    "print(x)\n",
    "print(y)\n",
    "print(z)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 📖Dictionaries\n",
    "Dictionaries are a very handle way to organize small amounts of data.They are extremely flexible mappings of keys to values, and form the basis of much of Python's internal implementation.\n",
    "They can be created via a comma-separated list of ``key:value`` pairs within curly braces:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "numbers = {'one':1, 'two':2, 'three':3}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Items are accessed and set via the indexing syntax used for lists and tuples, except here the index is not a zero-based order but valid key in the dictionary:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Access a value via the key\n",
    "numbers['two']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "New items can be added to the dictionary using indexing as well:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set a new key:value pair\n",
    "numbers['ninety'] = 90\n",
    "print(numbers)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Keep in mind that dictionaries do not maintain any sense of order for the input parameters; this is by design.\n",
    "This lack of ordering allows dictionaries to be implemented very efficiently, so that random element access is very fast, regardless of the size of the dictionary (if you're curious how this works, read about the concept of a *hash table*).\n",
    "The [python documentation](https://docs.python.org/3/library/stdtypes.html) has a complete list of the methods available for dictionaries."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "vvJJNl_jVUG4"
   },
   "source": [
    "### ✏️Task:\n",
    "Create and print a dictionary $Dic$ with the following key-value pairs:\n",
    "\n",
    "'book-129'\n",
    "\n",
    "'mouse-482'\n",
    "\n",
    "'monitor-37'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your solution here:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Sample solution\n",
    "\n",
    "Dic = {\n",
    "    'book': 129,\n",
    "    'mouse': 482,\n",
    "    'monitor': 37\n",
    "}\n",
    "\n",
    "print(Dic)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "yyzbBJr4VUG4"
   },
   "source": [
    "Add the key-value pair $'pc-733'$ to $Dic$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your solution here:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Sample solution\n",
    "\n",
    "Dic['pc'] = 733"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "l-y4pIZ3VUG5"
   },
   "source": [
    "Delete the key '$mouse$' and print the values of $Dic$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your solution here:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Sample solution\n",
    "\n",
    "del Dic['mouse']\n",
    "\n",
    "print(Dic.values())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "AgbnGs1FVUG5"
   },
   "source": [
    "Check whether '$book$' is a key of $Dic$ and if so, print its value.\n",
    "\n",
    "💡The `get()` method returns the value for a specified key if the key is in the dictionary, and None if the key is not found."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your solution here:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "id": "jqfjv1vpVUG5"
   },
   "outputs": [],
   "source": [
    "# Sample solution\n",
    "\n",
    "value = Dic.get('book')\n",
    "\n",
    "print(value)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "lKAb5uaoVUG5"
   },
   "source": [
    "Check whether '$mouse$' is a key of $Dic$.\n",
    "\n",
    "💡 You can check whether a key is in a dictionary by using `in`, this will return a Boolean value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your solution here:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "id": "hr8mh-cJVUG5",
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Sample solution\n",
    "\n",
    "print('mouse' in Dic)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 📖Sets\n",
    "\n",
    "The fourth basic collection is the set, which contains unordered collections of unique items.\n",
    "They are defined much like lists and tuples, except they use the curly brackets of dictionaries:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "primes = {2, 3, 5, 7}\n",
    "odds = {1, 3, 5, 7, 9}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you're familiar with the mathematics of sets, you'll be familiar with operations like the union, intersection, difference, symmetric difference, and others.\n",
    "Python's sets have all of these operations built-in, via methods or operators.\n",
    "For each, we'll show the two equivalent methods:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# union: items appearing in either\n",
    "primes | odds      # with an operator\n",
    "primes.union(odds) # equivalently with a method"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# intersection: items appearing in both\n",
    "primes & odds             # with an operator\n",
    "primes.intersection(odds) # equivalently with a method"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# difference: items in primes but not in odds\n",
    "primes - odds           # with an operator\n",
    "primes.difference(odds) # equivalently with a method"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# symmetric difference: items appearing in only one set\n",
    "primes ^ odds                     # with an operator\n",
    "primes.symmetric_difference(odds) # equivalently with a method"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Many more set methods and operations are available.\n",
    "You've probably already guessed what I'll say next: refer to Python's [online documentation](https://docs.python.org/3/library/stdtypes.html) for a complete reference."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "hsPqL1h2VUG6"
   },
   "source": [
    "### ✏️Task: \n",
    "\n",
    "Create two sets\n",
    "\n",
    "$X$ =  'Football', 'Swimming', 'Cycling', 'Basketball', 'Contortion'\n",
    "\n",
    "$Y$ =  'Hiking', 'Contortion', 'Chess', 'Volleyball', 'Cycling'\n",
    "\n",
    "and determine whether either $X$ or $Y$ contains the element $'Chess'$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your solution here:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "id": "hr8mh-cJVUG5",
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Sample solution\n",
    "\n",
    "X = {'Football', 'Swimming', 'Cycling', 'Basketball', 'Contortion'}\n",
    "Y = {'Hiking', 'Contortion', 'Chess', 'Volleyball', 'Cycling'}\n",
    "\n",
    "contains_chess = 'Chess' in X or 'Chess' in Y\n",
    "\n",
    "print(contains_chess)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "YkgOHWdPVUG6"
   },
   "source": [
    "Calculate and print the result of the following expressions\n",
    "\n",
    "$X \\, \\bigcup \\, Y$ (Union), \n",
    "$X \\, \\bigcap \\, Y$ (Intersection), \n",
    "$X \\, \\setminus \\, Y$ (Difference), \n",
    "$X \\, \\triangle \\, Y = (X \\, \\setminus \\, Y) \\, \\bigcup \\, (Y \\, \\setminus \\, X)$ (Symmetric difference)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your solution here:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "id": "Pq1B7FBIVUG6"
   },
   "outputs": [],
   "source": [
    "# Sample solution\n",
    "\n",
    "union_result = X | Y\n",
    "print(\"Union:\", union_result)\n",
    "\n",
    "intersection_result = X & Y\n",
    "print(\"Intersection:\", intersection_result)\n",
    "\n",
    "difference_result = X - Y\n",
    "print(\"Difference:\", difference_result)\n",
    "\n",
    "symmetric_difference_result = X ^ Y\n",
    "print(\"Symmetric Difference:\", symmetric_difference_result)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 🧠Dive Deeper\n",
    "\n",
    "Python contains several other data structures that you might find useful; these can generally be found in the built-in ``collections`` module. Once you've seen the standard built-in collection types, the use of these extended functionalities is very intuitive, and I'd suggest [reading about their use](https://docs.python.org/3/library/collections.html)."
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
