{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Control Flow"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*Control flow* is where the rubber really meets the road in programming.\n",
    "Without it, a program is simply a list of statements that are sequentially executed.\n",
    "With control flow, you can execute certain code blocks conditionally and/or repeatedly: these basic building blocks can be combined to create surprisingly sophisticated programs!\n",
    "\n",
    "Here we'll cover *conditional statements* (including \"``if``\", \"``elif``\", and \"``else``\"), *loop statements* (including \"``for``\" and \"``while``\" and the accompanying \"``break``\", \"``continue``\", and \"``pass``\")."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 📖Conditional Statements: ``if``-``elif``-``else``:\n",
    "Conditional statements, often referred to as *if-then* statements, allow the programmer to execute certain pieces of code depending on some Boolean condition.\n",
    "A basic example of a Python conditional statement is this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = -15\n",
    "\n",
    "if x == 0:\n",
    "    print(x, \"is zero\")\n",
    "elif x > 0:\n",
    "    print(x, \"is positive\")\n",
    "elif x < 0:\n",
    "    print(x, \"is negative\")\n",
    "else:\n",
    "    print(x, \"is unlike anything I've ever seen...\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note especially the use of colons (``:``) and whitespace to denote separate blocks of code.\n",
    "\n",
    "Python adopts the ``if`` and ``else`` often used in other languages; its more unique keyword is ``elif``, a contraction of \"else if\".\n",
    "In these conditional clauses, ``elif`` and ``else`` blocks are optional; additionally, you can optinally include as few or as many ``elif`` statements as you would like."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### ✏️Task:\n",
    "Define the function $f(x)$ to represent the polynomial $2x^2 + 3x + 1$. Once defined, modify the function using an `if-elif-else` statement to handle the following conditions:\n",
    "\n",
    "- Return the `f(x)`for: $$x < -1$$\n",
    "\n",
    "\n",
    "- Return the absolute value of `f(x)`for: $$-1 \\leq x \\leq -0.5$$\n",
    "\n",
    "\n",
    "- Return the `f(x)`for: $$-0.5 < x$$\n",
    "\n",
    "Print $f(-1)$, $f(-0.75)$ and $f(0)$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your solution here:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Sample solution\n",
    "\n",
    "def f(x):\n",
    "    y = 2 * x**2 + 3 * x + 1\n",
    "\n",
    "    if x < -1:\n",
    "        return y\n",
    "    elif -1 <= x <= -0.5:\n",
    "        return abs(y)\n",
    "    else:\n",
    "        return y\n",
    "\n",
    "print(f(-1))\n",
    "print(f(-0.75))\n",
    "print(f(0))  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 📖`for` loops\n",
    "Loops in Python are a way to repeatedly execute some code statement.\n",
    "So, for example, if we'd like to print each of the items in a list, we can use a ``for`` loop:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for N in [2, 3, 5, 7]:\n",
    "    print(N, end=' ') # print all on same line"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice the simplicity of the ``for`` loop: we specify the variable we want to use, the sequence we want to loop over, and use the \"``in``\" operator to link them together in an intuitive and readable way.\n",
    "More precisely, the object to the right of the \"``in``\" can be any Python *iterator*.\n",
    "An iterator can be thought of as a generalized sequence, and we'll discuss them in [Iterators](10-Iterators.ipynb).\n",
    "\n",
    "For example, one of the most commonly-used iterators in Python is the ``range`` object, which generates a sequence of numbers:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for i in range(10):\n",
    "    print(i, end=' ')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the range starts at zero by default, and that by convention the top of the range is not included in the output.\n",
    "Range objects can also have more complicated values:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# range from 5 to 10\n",
    "list(range(5, 10))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# range from 0 to 10 by 2\n",
    "list(range(0, 10, 2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You might notice that the meaning of ``range`` arguments is very similar to the slicing syntax that we covered in [Lists](06-Built-in-Data-Structures.ipynb#Lists)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "cJ-ktccnVUG8"
   },
   "source": [
    "### ✏️Task:\n",
    "Define the following function: $$g(x) = x^2 - 3x + 2 $$\n",
    "\n",
    "Use a for-loop and the $range$ function to print the values of `g` for `x` between -19 and 37 with step size 5."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "id": "vR1oqbdbVUG8"
   },
   "outputs": [],
   "source": [
    "# Your solution here:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Sample solution\n",
    "\n",
    "def g(x):\n",
    "    return x**2 - 3*x + 2\n",
    "\n",
    "for x in range(-19, 38, 5): \n",
    "    print('(', x, ',', g(x), ')')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "HfzbiDdaVUG9"
   },
   "source": [
    "### ✏️Task:\n",
    "Create a dictionary named `student_grades`with following key values: $John$: 85, $Emily$: 90, $Sarah$: 97, $Michael$: 92, $David$: 88.\n",
    "\n",
    "Loop over the `student_grades` dictionary using a for-loop and print out the results.\n",
    "\n",
    "💡You can loop over every sequential type in Python."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "id": "vR1oqbdbVUG8"
   },
   "outputs": [],
   "source": [
    "# Your solution here:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Sample solution\n",
    "\n",
    "student_grades = {\n",
    "    'John': 85,\n",
    "    'Emily': 90,\n",
    "    'Sarah': 97,\n",
    "    'Michael': 92,\n",
    "    'David': 88\n",
    "}\n",
    "\n",
    "for name in student_grades:\n",
    "    grade = student_grades[name]\n",
    "    print(\"Name:\", name, \"Grade:\", grade)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 📖`while` loops\n",
    "The other type of loop in Python is a ``while`` loop, which iterates until some condition is met:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "i = 0\n",
    "while i < 10:\n",
    "    print(i, end=' ')\n",
    "    i += 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The argument of the ``while`` loop is evaluated as a boolean statement, and the loop is executed until the statement evaluates to False."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "igYV58yDVUG-"
   },
   "source": [
    "### ✏️Task:\n",
    "Write a `while` loop to print out $h(x)$ starting from $x=0$ until the function $h$ returns a value less than or equal to $73$. Define the function $h(x)$ to be $3x - 2$. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your solution here:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "id": "vVH1a-2vVUG-"
   },
   "outputs": [],
   "source": [
    "# Sample solution\n",
    "\n",
    "def h(x):\n",
    "    return 3 * x - 2\n",
    "\n",
    "x = 0\n",
    "while h(x) <= 73:\n",
    "    print(h(x))\n",
    "    x += 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 📖``break`` and ``continue``: Fine-Tuning Your Loops\n",
    "There are two useful statements that can be used within loops to fine-tune how they are executed:\n",
    "\n",
    "- The ``break`` statement breaks-out of the loop entirely\n",
    "- The ``continue`` statement skips the remainder of the current loop, and goes to the next iteration\n",
    "\n",
    "These can be used in both ``for`` and ``while`` loops.\n",
    "\n",
    "Here is an example of using ``continue`` to print a string of odd numbers.\n",
    "In this case, the result could be accomplished just as well with an ``if-else`` statement, but sometimes the ``continue`` statement can be a more convenient way to express the idea you have in mind:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for n in range(20):\n",
    "    # if the remainder of n / 2 is 0, skip the rest of the loop\n",
    "    if n % 2 == 0:\n",
    "        continue\n",
    "    print(n, end=' ')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is an example of a ``break`` statement used for a less trivial task.\n",
    "This loop will fill a list with all Fibonacci numbers up to a certain value:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a, b = 0, 1\n",
    "amax = 100\n",
    "L = []\n",
    "\n",
    "while True:\n",
    "    (a, b) = (b, a + b)\n",
    "    if a > amax:\n",
    "        break\n",
    "    L.append(a)\n",
    "\n",
    "print(L)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that we use a ``while True`` loop, which will loop forever unless we have a break statement!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "J6lKYWgQVUG8"
   },
   "source": [
    "### ✏️Task:\n",
    "Create a list with all the multiples of 3 between -13 and 13. Write a loop to iterate through this list. Use the `continue` statement to skip processing if the current list element is even, print the current list element if it is odd, and use the `break` statement to exit the loop once the current list element exceeds 11.\n",
    "💡You can use a list comprehension to create a list of multiples of 3 in the given range."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your solution here:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "id": "vVH1a-2vVUG-"
   },
   "outputs": [],
   "source": [
    "# Sample solution\n",
    "\n",
    "multiples_of_three = [x for x in range(-13, 14) if x % 3 == 0]\n",
    "\n",
    "for number in multiples_of_three:\n",
    "    if number % 2 == 0:\n",
    "        continue\n",
    "    if number > 11:\n",
    "        break\n",
    "    print(number)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 📖Loops with an ``else`` Block\n",
    "One rarely used pattern available in Python is the ``else`` statement as part of a ``for`` or ``while`` loop.\n",
    "We discussed the ``else`` block earlier: it executes if all the ``if`` and ``elif`` statements evaluate to ``False``.\n",
    "The loop-``else`` is perhaps one of the more confusingly-named statements in Python; I prefer to think of it as a ``nobreak`` statement: that is, the ``else`` block is executed only if the loop ends naturally, without encountering a ``break`` statement.\n",
    "\n",
    "As an example of where this might be useful, consider the following (non-optimized) implementation of the *Sieve of Eratosthenes*, a well-known algorithm for finding prime numbers:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "L = []\n",
    "nmax = 30\n",
    "\n",
    "for n in range(2, nmax):\n",
    "    for factor in L:\n",
    "        if n % factor == 0:\n",
    "            break\n",
    "    else: # no break\n",
    "        L.append(n)\n",
    "print(L)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### ✏️Task:\n",
    "Solve the previous task using an `else` block."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your solution here:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "id": "vVH1a-2vVUG-"
   },
   "outputs": [],
   "source": [
    "# Sample solution\n",
    "\n",
    "multiples_of_three = [x for x in range(-13, 14) if x % 3 == 0]\n",
    "\n",
    "for number in multiples_of_three:\n",
    "    if number % 2 == 0:\n",
    "        continue\n",
    "    elif number > 11:\n",
    "        break\n",
    "    else:\n",
    "        print(number)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The ``else`` statement only executes if none of the factors divide the given number.\n",
    "The ``else`` statement works similarly with the ``while`` loop."
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
