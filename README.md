### About This Course

This course is designed to give newcomers a strong start in the world of Python programming. In this course, we break down the basics step by step, ensuring that each concept is presented in a straightforward and easy-to-grasp manner. You'll get to understand Python's structure, how to craft simple commands, and write your first codes. We believe in learning by doing, so expect a mix of clear explanations followed by hands-on activities. Whether you're looking to shift careers, enhance your current skills, or simply satisfy a curiosity, "Python Onboarding" offers a clear and engaging entry point into the world of coding with Python.

### Level

Introductory

### Requirements

This course is designed for beginners, so you don't need any prior experience with Python or coding.

### Subject Area

Geoinformatics

### What You Will Learn

- Installation and Practical Considerations
- How to Run Python Code
- Python Language Syntax
- Basic Python Semantics: Variables and Objects
- Built-In Types: Simple Values
- Basic Python Semantics: Operators
- Built-In Data Structures
- Defining and Using Functions and Classes
- Control Flow
- Errors and Exceptions
- Iterators
- List Comprehensions
- Generators
- Modules and Packages
- Strings and Regular Expressions
- A Preview of Data Science Tools

### Resources

[Whirlwind Tour of Python](https://github.com/jakevdp/WhirlwindTourOfPython) by Jake VanderPlas  
Farzaneh Sadeghi

### Administration

Farzaneh Sadeghi

---

This content is based on "[Whirlwind Tour of Python](https://github.com/jakevdp/WhirlwindTourOfPython)" by Jake VanderPlas, which is licensed under a [CC0](https://creativecommons.org/publicdomain/zero/1.0/). Modifications were made to the original content. This modified content is licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/).
